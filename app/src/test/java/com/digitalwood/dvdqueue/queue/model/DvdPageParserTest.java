package com.digitalwood.dvdqueue.queue.model;

import com.digitalwood.dvdqueue.model.Movie;
import com.digitalwood.dvdqueue.queue.model.storage.WebSource;

import org.junit.Test;

import java.util.List;

import static org.junit.Assert.*;

/**
 * Created by Andrew on 11/27/2016.
 * Copyright 2016
 */

public class DvdPageParserTest {

    private static final String ONE_MOVIE_HTML =
            WebSource.CACHED_RESPONSE_TOP +
            "<tr><td class='dvdcell'><a href='/movies/6534/star-trek-beyond-2016'><img class='movieimg' alt='Star Trek Beyond DVD Release Date' title='Star Trek Beyond DVD Release Date' src='http://s0.dvdsreleasedates.com/posters/110/S/Star-Trek-Beyond-2016.jpg'/></a><br/><a style='color:#000;' href='/movies/6534/star-trek-beyond-2016'>Star Trek Beyond</a><br/><table class='celldiscs'><tr><td class='imdblink left'>imdb: <a href='http://www.imdb.com/title/tt2660888/' target='_blank' rel='nofollow'>7.2</a></td><td class='imdblink right'>PG-13&nbsp;&nbsp;</td></tr></table></td>\n" +
            WebSource.CACHED_RESPONSE_BOTTOM;

    @Test
    public void getMovieList_onDefaultParser_returnsEmptyList() {
        DvdPageParser parser = new DvdPageParser();

        List<Movie> movieList = parser.getMovieList();

        assertTrue(movieList.isEmpty());
    }

    @Test
    public void getMovieList_withHtmlWithOneMovie_returnsOneMovie() {
        DvdPageParser parser = new DvdPageParser(ONE_MOVIE_HTML);

        List<Movie> movieList = parser.getMovieList();

        assertEquals(1, movieList.size());
    }

    @Test
    public void getMovieList_withCachedPage_returns49Movies() {
        DvdPageParser parser = new DvdPageParser();
        parser.setHtml(WebSource.CACHED_RESPONSE);

        List<Movie> movieList = parser.getMovieList();

        assertEquals(49, movieList.size());
    }
}
