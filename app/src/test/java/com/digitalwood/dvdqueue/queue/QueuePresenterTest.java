package com.digitalwood.dvdqueue.queue;

import android.support.annotation.NonNull;

import com.digitalwood.dvdqueue.model.Movie;
import com.digitalwood.dvdqueue.queue.model.LoadMoviesCallback;
import com.digitalwood.dvdqueue.queue.model.QueueModel;

import org.junit.Test;
import org.mockito.ArgumentCaptor;

import java.util.Arrays;
import java.util.List;

import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.*;

/**
 * Created by Andrew on 11/25/2016.
 * Copyright 2016
 */

public class QueuePresenterTest {

    private QueueView mockView;
    private QueueModel mockModel;
    private static final List<Movie> ONE_MOVIE = Arrays.asList(new Movie());
    private static final Movie SAMPLE_MOVIE = new Movie("Office Space", "", "1999-02-19", "7.8", "R");

    @NonNull
    private QueuePresenter getQueuePresenterWithMocks() {
        mockView = mock(QueueView.class);
        mockModel = mock(QueueModel.class);
        return new QueuePresenter(mockView, mockModel);
    }

    @Test
    public void subscribe_loadsMovieList() {
        QueuePresenter presenter = getQueuePresenterWithMocks();

        presenter.subscribe();

        verify(mockModel).loadMovies(any(LoadMoviesCallback.class));
    }

    @Test
    public void subscribe_ifMoviesLoadedSuccessfully_setsMovieList() {
        QueuePresenter presenter = getQueuePresenterWithMocks();
        ArgumentCaptor<LoadMoviesCallback> arg = ArgumentCaptor.forClass(LoadMoviesCallback.class);
        doNothing().when(mockModel).loadMovies(arg.capture());

        presenter.subscribe();
        arg.getValue().onSuccess(ONE_MOVIE);

        verify(mockView).setMovieList(ONE_MOVIE);
    }

    @Test
    public void onMovieClicked_launchesBrowserForThatMovie() {
        QueuePresenter presenter = getQueuePresenterWithMocks();
        ArgumentCaptor<String> arg = ArgumentCaptor.forClass(String.class);

        presenter.onMovieClicked(SAMPLE_MOVIE);

        verify(mockView).launchBrowser(arg.capture());
        assertTrue(arg.getValue().contains(SAMPLE_MOVIE.getTitle()));
    }
}
