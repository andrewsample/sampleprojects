package com.digitalwood.dvdqueue.queue;

import android.support.test.annotation.UiThreadTest;
import android.support.test.runner.AndroidJUnit4;

import com.digitalwood.dvdqueue.QueueActivity;
import com.digitalwood.dvdqueue.model.Movie;

import org.junit.*;
import org.junit.runner.*;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static org.junit.Assert.assertEquals;

/**
 * Created by Andrew on 11/25/2016.
 * Copyright 2016
 */
@RunWith(AndroidJUnit4.class)
public class QueueActivityTest {

    private static final List<Movie> NO_MOVIES = new ArrayList<>();
    private static final List<Movie> ONE_MOVIE = Arrays.asList(new Movie());
    private static final List<Movie> TWO_MOVIES = Arrays.asList(new Movie(), new Movie());

    @Rule
    public QueueActivityTestRule<QueueActivity> mActivityRule = new QueueActivityTestRule<>(
            QueueActivity.class);


    @Test
    @UiThreadTest
    public void setMovieList_WithNoMovies_DisplaysEmptyList() {
        final QueueActivity activity = mActivityRule.getActivity();

        activity.setMovieList(NO_MOVIES);

        assertEquals(0, mActivityRule.movieListView.getAdapter().getItemCount());
    }

    @Test
    @UiThreadTest
    public void setMovieList_WithOneMovie_DisplaysOneMovie() {
        final QueueActivity activity = mActivityRule.getActivity();

        activity.setMovieList(ONE_MOVIE);

        assertEquals(1, mActivityRule.movieListView.getAdapter().getItemCount());
    }

    @Test
    @UiThreadTest
    public void setMovieList_WithTwoMovies_DisplaysTwoMovie() {
        final QueueActivity activity = mActivityRule.getActivity();

        activity.setMovieList(TWO_MOVIES);

        assertEquals(2, mActivityRule.movieListView.getAdapter().getItemCount());
    }
}
