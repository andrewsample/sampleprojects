package com.digitalwood.dvdqueue.queue;

import android.support.test.rule.ActivityTestRule;
import android.support.v7.widget.RecyclerView;

import com.digitalwood.dvdqueue.QueueActivity;
import com.digitalwood.dvdqueue.R;

/**
 * Created by Andrew on 11/26/2016.
 * Copyright 2016
 */

public class QueueActivityTestRule<A extends QueueActivity> extends ActivityTestRule<A>  {

    // Tests. Just expose them. Don't bother with getters.
    public RecyclerView movieListView;

    public QueueActivityTestRule(Class<A> activityClass) {
        super(activityClass);
    }

    @Override
    protected void afterActivityLaunched() {
        super.afterActivityLaunched();
        final QueueActivity activity = getActivity();
        movieListView = (RecyclerView) activity.findViewById(R.id.queueMovieList);
    }
}
