package com.digitalwood.dvdqueue.queue.model.storage;

import android.content.Context;
import android.support.annotation.NonNull;

/**
 * Created by Andrew on 12/11/2016.
 * Copyright 2016
 */

public class TestDbSource extends DatabaseSource {

    public static final String DB_NAME = "TEST.db";

    public TestDbSource(Context appContext) {
        super(appContext);
    }

    @NonNull
    @Override
    protected MovieListDbHelper getMovieListDbHelper() {
        return new MovieListDbHelper(appContext, DB_NAME);
    }
}
