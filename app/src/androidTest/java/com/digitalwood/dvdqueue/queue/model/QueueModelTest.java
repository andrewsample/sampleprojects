package com.digitalwood.dvdqueue.queue.model;

import android.support.test.runner.AndroidJUnit4;

import com.digitalwood.dvdqueue.queue.model.storage.DatabaseSource;
import com.digitalwood.dvdqueue.queue.model.storage.WebSource;

import org.junit.Test;
import org.junit.runner.RunWith;

import static android.support.test.InstrumentationRegistry.getTargetContext;
import static org.junit.Assert.*;
import static org.mockito.Matchers.*;
import static org.mockito.Mockito.*;

/**
 * Created by Andrew on 12/9/2016.
 * Copyright 2016
 */

@RunWith(AndroidJUnit4.class)
public class QueueModelTest {

    @Test
    public void loadMovies_IfSynchedRecently_LoadsFromDatabase() {
        LoadMoviesCallback mockCbk = mock(LoadMoviesCallback.class);
        DatabaseSource mockDbStorage = mock(DatabaseSource.class);
        when(mockDbStorage.getMsSinceLastSync()).thenReturn(0L);
        WebSource mockWebSource = mock(WebSource.class);
        QueueModel model = new QueueModel(getTargetContext(), mockDbStorage, mockWebSource);

        model.loadMovies(mockCbk);

        assertTrue(model.wasLastLoadDoneFromDatabase());
        verify(mockDbStorage).loadMovies(any(LoadMoviesCallback.class));
    }

    @Test
    public void loadMovies_IfHasNotSynchedRecently_LoadsFromWeb() {
        LoadMoviesCallback mockCbk = mock(LoadMoviesCallback.class);
        DatabaseSource mockDbStorage = mock(DatabaseSource.class);
        when(mockDbStorage.getMsSinceLastSync()).thenReturn(Long.MAX_VALUE);
        WebSource mockWebSource = mock(WebSource.class);
        QueueModel model = new QueueModel(getTargetContext(), mockDbStorage, mockWebSource);

        model.loadMovies(mockCbk);

        assertFalse(model.wasLastLoadDoneFromDatabase());
        verify(mockWebSource).loadMovies(any(LoadMoviesCallback.class));
    }
}
