package com.digitalwood.dvdqueue.queue.model.storage;

import android.content.Context;
import android.support.test.runner.AndroidJUnit4;

import com.digitalwood.dvdqueue.model.Movie;
import com.digitalwood.dvdqueue.queue.model.LoadMoviesCallback;

import org.junit.Test;
import org.junit.runner.RunWith;

import java.util.Arrays;
import java.util.List;

import static android.support.test.InstrumentationRegistry.getTargetContext;
import static junit.framework.Assert.assertEquals;
import static junit.framework.Assert.assertTrue;

/**
 * Created by Andrew on 12/11/2016.
 * Copyright 2016
 */

@RunWith(AndroidJUnit4.class)
public class DatabaseSourceTest {
    private volatile boolean callbackFired;
    private boolean calledOnSuccess;
    private List<Movie> onSuccessMovieList;
    private boolean calledOnError;
    private String onErrorMessage;
    private Movie ONE_MOVIE = new Movie("1", "", "", "", "");
    private Movie ANOTHER_MOVIE = new Movie("2", "", "", "", "");
    private List<Movie> LIST_WITH_ONE_MOVIE = Arrays.asList( ONE_MOVIE );
    private List<Movie> LIST_WITH_TWO_MOVIES = Arrays.asList( ONE_MOVIE, ANOTHER_MOVIE );

    @Test
    public void saveMovies_WithOneMovie_TotalsOneMovie() {
        TestDbSource dbSource = createNewEmptyTestDatabase();

        dbSource.saveMovies(LIST_WITH_ONE_MOVIE);

        dbSource.loadMovies(testCallback);
        waitForCallback();
        assertTrue(calledOnSuccess);
        assertEquals(1, onSuccessMovieList.size());
    }

    @Test
    public void saveMovies_CalledTwiceWithSameMovie_TotalsOneMovie() {
        TestDbSource dbSource = createNewEmptyTestDatabase();

        dbSource.saveMovies(LIST_WITH_ONE_MOVIE);
        dbSource.saveMovies(LIST_WITH_ONE_MOVIE);

        dbSource.loadMovies(testCallback);
        waitForCallback();
        assertTrue(calledOnSuccess);
        assertEquals(1, onSuccessMovieList.size());
    }

    @Test
    public void loadMovies_WithReleaseDatesSet_SortsByReleaseDateDescending() {
        TestDbSource dbSource = createNewEmptyTestDatabase();
        Movie oldOne = new Movie("1", null, "2015-01-01", "", "");
        dbSource.saveMovies(Arrays.asList(oldOne));
        Movie olderOne = new Movie("2", null, "1982-01-01", "", "");
        dbSource.saveMovies(Arrays.asList(olderOne));
        Movie newOne = new Movie("3", null, "2016-01-01", "", "");
        dbSource.saveMovies(Arrays.asList(newOne));

        dbSource.loadMovies(testCallback);

        waitForCallback();
        assertTrue(calledOnSuccess);
        assertEquals(3, onSuccessMovieList.size());
        assertEquals(newOne.getTitle(), onSuccessMovieList.get(0).getTitle()); // newest is first
        assertEquals(oldOne.getTitle(), onSuccessMovieList.get(1).getTitle());
        assertEquals(olderOne.getTitle(), onSuccessMovieList.get(2).getTitle()); // oldest is last
    }

    @Test
    public void loadMovies_MultipleMoviesAtATime_SortsByReleaseDateDescendingAndByInsertOrder() {
        TestDbSource dbSource = createNewEmptyTestDatabase();
        Movie oldOne1 = new Movie("1", null, "2015-01-01", "", "");
        Movie oldOne2 = new Movie("2", null, "2015-01-01", "", "");
        Movie oldOne3 = new Movie("3", null, "2015-01-01", "", "");
        dbSource.saveMovies(Arrays.asList(oldOne1, oldOne2, oldOne3));
        Movie olderOne1 = new Movie("Z", null, "1982-01-01", "", "");
        Movie olderOne2 = new Movie("Y", null, "1982-01-01", "", "");
        Movie olderOne3 = new Movie("X", null, "1982-01-01", "", "");
        dbSource.saveMovies(Arrays.asList(olderOne1, olderOne2, olderOne3));
        Movie newOne1 = new Movie("HHH", null, "2016-01-01", "", "");
        Movie newOne2 = new Movie("AAA", null, "2016-01-01", "", "");
        Movie newOne3 = new Movie("III", null, "2016-01-01", "", "");
        dbSource.saveMovies(Arrays.asList(newOne1, newOne2, newOne3));

        dbSource.loadMovies(testCallback);

        waitForCallback();
        assertTrue(calledOnSuccess);
        assertEquals(9, onSuccessMovieList.size());
        assertEquals(newOne1.getTitle(), onSuccessMovieList.get(0).getTitle()); // newest is first
        assertEquals(newOne2.getTitle(), onSuccessMovieList.get(1).getTitle()); // newest is first
        assertEquals(newOne3.getTitle(), onSuccessMovieList.get(2).getTitle()); // newest is first
        assertEquals(oldOne1.getTitle(), onSuccessMovieList.get(3).getTitle());
        assertEquals(oldOne2.getTitle(), onSuccessMovieList.get(4).getTitle());
        assertEquals(oldOne3.getTitle(), onSuccessMovieList.get(5).getTitle());
        assertEquals(olderOne1.getTitle(), onSuccessMovieList.get(6).getTitle()); // oldest is last
        assertEquals(olderOne2.getTitle(), onSuccessMovieList.get(7).getTitle()); // oldest is last
        assertEquals(olderOne3.getTitle(), onSuccessMovieList.get(8).getTitle()); // oldest is last
    }

    @Test
    public void removeMovieFromQueue_OneMinusOne_ReturnsZeroMovies() {
        TestDbSource dbSource = createNewEmptyTestDatabase();

        dbSource.saveMovies(LIST_WITH_ONE_MOVIE);
        dbSource.removeMovieFromQueue(ONE_MOVIE);
        dbSource.loadMovies(testCallback);

        waitForCallback();
        assertTrue(calledOnSuccess);
        assertEquals(0, onSuccessMovieList.size());
    }

    // The idea behind this is we mark movies as deleted in the db but don't call delete.
    // This way, if the same movie shows up later, we won't save it again, and the user
    // won't have to delete it again.
    @Test
    public void removeMovieFromQueue_WhenMovieIsAddedBack_StillLoadsZeroMovies() {
        TestDbSource dbSource = createNewEmptyTestDatabase();

        dbSource.saveMovies(LIST_WITH_ONE_MOVIE);
        dbSource.removeMovieFromQueue(ONE_MOVIE);
        dbSource.saveMovies(LIST_WITH_ONE_MOVIE);
        dbSource.loadMovies(testCallback);

        waitForCallback();
        assertTrue(calledOnSuccess);
        assertEquals(0, onSuccessMovieList.size());
    }

    @Test
    public void removeMovieFromQueue_WhenMovieIsNotInTheDb_MakesNoChanges() {
        TestDbSource dbSource = createNewEmptyTestDatabase();

        dbSource.saveMovies(LIST_WITH_ONE_MOVIE);
        dbSource.removeMovieFromQueue(ANOTHER_MOVIE);
        dbSource.loadMovies(testCallback);

        waitForCallback();
        assertTrue(calledOnSuccess);
        assertEquals(1, onSuccessMovieList.size());
    }

    @Test
    public void removeMovieFromQueue_CalledOnTheSameMovieTwice_RemovesItOnce() {
        TestDbSource dbSource = createNewEmptyTestDatabase();

        dbSource.saveMovies(LIST_WITH_TWO_MOVIES);
        dbSource.removeMovieFromQueue(ONE_MOVIE);
        dbSource.removeMovieFromQueue(ONE_MOVIE);
        dbSource.removeMovieFromQueue(ONE_MOVIE);
        dbSource.loadMovies(testCallback);

        waitForCallback();
        assertTrue(calledOnSuccess);
        assertEquals(1, onSuccessMovieList.size());
    }


    TestDbSource createNewEmptyTestDatabase() {
        Context context = getTargetContext();
        context.deleteDatabase(TestDbSource.DB_NAME);
        TestDbSource dbSource = new TestDbSource(context);
        return dbSource;
    }


    LoadMoviesCallback testCallback = new LoadMoviesCallback() {
        @Override
        public void onSuccess(List<Movie> movieList) {
            calledOnSuccess = true;
            onSuccessMovieList = movieList;
            callbackFired = true;
        }

        @Override
        public void onError(String message) {
            calledOnError = true;
            onErrorMessage = message;
            callbackFired = true;
        }
    };

    private void waitForCallback() {
        while (!callbackFired) {
            try {
                Thread.sleep(500);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }
}
