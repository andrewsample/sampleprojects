package com.digitalwood.dvdqueue;

/**
 * Created by Andrew on 11/25/2016.
 * Copyright 2016
 */

public interface BasePresenter {
    void subscribe();
    void unsubscribe();
}
