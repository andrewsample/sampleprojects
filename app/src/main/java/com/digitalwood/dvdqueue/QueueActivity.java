package com.digitalwood.dvdqueue;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.support.v7.widget.helper.ItemTouchHelper;
import android.view.View;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Toast;

import com.digitalwood.dvdqueue.model.Movie;
import com.digitalwood.dvdqueue.queue.OnMovieClickListener;
import com.digitalwood.dvdqueue.queue.QueueAdapter;
import com.digitalwood.dvdqueue.queue.model.QueueModel;
import com.digitalwood.dvdqueue.queue.QueuePresenter;
import com.digitalwood.dvdqueue.queue.QueueView;

import java.util.List;

import static android.content.Intent.ACTION_VIEW;

public class QueueActivity
        extends AppCompatActivity
        implements QueueView {

    private QueuePresenter presenter;
    private RecyclerView movieListView;
    private QueueAdapter movieListAdapter;
    private RecyclerView.LayoutManager movieListLayoutManager;

    @Override
    protected void onResume() {
        super.onResume();
        presenter.subscribe();
    }

    @Override
    protected void onPause() {
        super.onPause();
        presenter.unsubscribe();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_queue);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
            }
        });

        movieListView = (RecyclerView) findViewById(R.id.queueMovieList);
        movieListLayoutManager = new LinearLayoutManager(this);
        movieListView.setLayoutManager(movieListLayoutManager);

        ItemTouchHelper.SimpleCallback swipeCallback = createSwipeCallback();
        ItemTouchHelper swipeDetector = new ItemTouchHelper(swipeCallback);
        swipeDetector.attachToRecyclerView(movieListView);

        presenter = new QueuePresenter(this, new QueueModel(this.getApplicationContext()));
    }

    private ItemTouchHelper.SimpleCallback createSwipeCallback() {
        ItemTouchHelper.SimpleCallback swipeCallback = new ItemTouchHelper.SimpleCallback(0, ItemTouchHelper.LEFT | ItemTouchHelper.RIGHT) {
            @Override
            public boolean onMove(RecyclerView recyclerView, RecyclerView.ViewHolder viewHolder, RecyclerView.ViewHolder target) {
                return false;
            }

            @Override
            public void onSwiped(RecyclerView.ViewHolder viewHolder, int direction) {
                presenter.onSwipedMovie(viewHolder.getAdapterPosition());
            }
        };

        return swipeCallback;
    }

    @Override
    public void setMovieList(List<Movie> movieList) {
        movieListAdapter = new QueueAdapter(movieList);
        movieListAdapter.setOnMovieClickListener(new OnMovieClickListener() {
            @Override
            public void onClick(Movie movie) {
                presenter.onMovieClicked(movie);
            }
        });
        movieListView.setAdapter(movieListAdapter);
    }

    @Override
    public void launchBrowser(String urlToEncode) {
        Uri uri = Uri.parse(urlToEncode);
        Intent intent = new Intent(ACTION_VIEW, uri);
        startActivity(intent);
//        Snackbar.make(movieListView, url.getTitle(), Snackbar.LENGTH_SHORT)
//                .setAction("Action", null).show();
    }

    @Override
    public Movie getMovieAtPosition(int adapterPosition) {
        return movieListAdapter.getItemAt(adapterPosition);
    }

    @Override
    public void removeMovieAtPosition(int adapterPosition) {
        movieListAdapter.remove(adapterPosition);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_queue, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
