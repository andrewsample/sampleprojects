package com.digitalwood.dvdqueue.model;

/**
 * Created by Andrew on 11/26/2016.
 * Copyright 2016
 */

public class Movie {
    private String title;
    private String imgUrl;
    private String releaseDate;
    private String imdbScore;
    private String parentalRating;

    // NOTE: Picasso handles null URLs , but throws an IllegalArgumentException on an empty string.
    // So I use null as the default imgUrl.
    public Movie() {
        this("", null, "", "", "N/A");
    }

    public Movie(String title, String imgUrl, String releaseDate, String imdbScore, String parentalRating) {
        this.title = title;
        this.imgUrl = imgUrl;
        this.releaseDate = releaseDate;
        this.imdbScore = imdbScore;
        this.parentalRating = parentalRating;
    }

    public String getTitle() {
        return title;
    }

    public Movie setTitle(String title) {
        this.title = title;
        return this;
    }

    public String getImgUrl() {
        return imgUrl;
    }

    public Movie setImgUrl(String imgUrl) {
        this.imgUrl = imgUrl;
        return this;
    }

    public String getReleaseDate() {
        return releaseDate;
    }

    public Movie setReleaseDate(String releaseDate) {
        this.releaseDate = releaseDate;
        return this;
    }

    public String getImdbScore() {
        return imdbScore;
    }

    public Movie setImdbScore(String imdbScore) {
        this.imdbScore = imdbScore;
        return this;
    }

    public String getParentalRating() {
        return parentalRating;
    }

    public Movie setParentalRating(String parentalRating) {
        this.parentalRating = parentalRating;
        return this;
    }
}
