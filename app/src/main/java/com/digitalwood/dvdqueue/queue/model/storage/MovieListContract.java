package com.digitalwood.dvdqueue.queue.model.storage;

import android.provider.BaseColumns;

/**
 * Created by Andrew on 12/9/2016.
 * Copyright 2016
 */

public class MovieListContract {
    // To prevent someone from accidentally instantiating the contract class,
    // make the constructor private.
    private MovieListContract() {}

    /* Inner class that defines the table contents */
    public static class MovieTable implements BaseColumns {
        public static final String TABLE_NAME = "movie";
        public static final String COLUMN_NAME_TITLE = "title";
        public static final String COLUMN_NAME_IMG_URL = "img_url";
        public static final String COLUMN_NAME_RELEASE_DATE = "release_week";
        public static final String COLUMN_NAME_IMDB_SCORE = "imdb_score";
        public static final String COLUMN_NAME_PARENTAL_RATING = "parental_rating";
        public static final String COLUMN_NAME_REMOVED = "removed";
    }

    public static class SyncTable implements BaseColumns {
        public static final String TABLE_NAME = "sync";
        public static final String COLUMN_NAME_LAST_SYNC = "last_sync";
    }
}
