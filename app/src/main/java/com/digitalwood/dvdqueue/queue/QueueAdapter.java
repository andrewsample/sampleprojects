package com.digitalwood.dvdqueue.queue;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.digitalwood.dvdqueue.R;
import com.digitalwood.dvdqueue.model.Movie;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Andrew on 11/25/2016.
 * Copyright 2016
 */

public class QueueAdapter extends RecyclerView.Adapter<QueueAdapter.ViewHolder> {

    private static List<Movie> queueMovieList = new ArrayList<>();
    private static OnMovieClickListener movieClickedListener;

    public static class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        protected TextView title;
        protected ImageView img;
        protected TextView imdbScore;
        protected TextView parentalRating;
        protected TextView releaseDate;

        public ViewHolder(View v) {
            super(v);
            v.setOnClickListener(this);
            title = (TextView) v.findViewById(R.id.queueItemTitle);
            img = (ImageView) v.findViewById(R.id.queueItemImage);
            imdbScore = (TextView) v.findViewById(R.id.queueItemImdbScore);
            parentalRating = (TextView) v.findViewById(R.id.queueItemParentalRating);
            releaseDate = (TextView) v.findViewById(R.id.queueItemReleaseDate);
        }

        @Override
        public void onClick(View v) {
            if (movieClickedListener != null) {
                Movie movie = queueMovieList.get(getAdapterPosition());
                movieClickedListener.onClick(movie);
            }
        }
    }

    public QueueAdapter(@NonNull List<Movie> queueMovieList) {
        this.queueMovieList = queueMovieList;
    }

    @Override
    public QueueAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext())
                               .inflate(R.layout.item_queue, parent, false);
        return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(QueueAdapter.ViewHolder holder, int position) {
        Movie movie = queueMovieList.get(position);
        holder.title.setText(movie.getTitle());
        holder.imdbScore.setText(movie.getImdbScore());
        holder.parentalRating.setText(movie.getParentalRating());
        holder.releaseDate.setText(movie.getReleaseDate());
        Picasso.with(holder.img.getContext()).load(movie.getImgUrl()).into(holder.img);
    }

    @Override
    public int getItemCount() {
        return queueMovieList.size();
    }

    public void setOnMovieClickListener(OnMovieClickListener movieClickedListener) {
        this.movieClickedListener = movieClickedListener;
    }

    public Movie getItemAt(int adapterPosition) {
        return queueMovieList.get(adapterPosition);
    }

    public void remove(int adapterPosition) {
        queueMovieList.remove(adapterPosition);
        notifyItemRemoved(adapterPosition);
    }
}
