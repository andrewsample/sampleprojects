package com.digitalwood.dvdqueue.queue;

import android.net.Uri;

import com.digitalwood.dvdqueue.BasePresenter;
import com.digitalwood.dvdqueue.model.Movie;
import com.digitalwood.dvdqueue.queue.model.LoadMoviesCallback;
import com.digitalwood.dvdqueue.queue.model.QueueModel;

import java.util.List;

/**
 * Created by Andrew on 11/25/2016.
 * Copyright 2016
 */

public class QueuePresenter implements BasePresenter {
    private QueueView view;
    private QueueModel model;

    public QueuePresenter(QueueView view, QueueModel model) {
        this.view = view;
        this.model = model;
    }

    @Override
    public void subscribe() {
        model.loadMovies(new LoadMoviesCallback() {
            @Override
            public void onSuccess(List<Movie> movieList) {
                view.setMovieList(movieList);
            }

            @Override
            public void onError(String message) {
//                view.displayError(message);
            }
        });
    }

    @Override
    public void unsubscribe() {

    }

    public void onMovieClicked(Movie movie) {
        String webPage = "http://www.redbox.com/search?q=" + movie.getTitle() + "&d=";
        view.launchBrowser(webPage);
    }

    public void onSwipedMovie(int adapterPosition) {
        Movie movie = view.getMovieAtPosition(adapterPosition);
        model.removeMovie(movie);
        view.removeMovieAtPosition(adapterPosition);
    }
}
