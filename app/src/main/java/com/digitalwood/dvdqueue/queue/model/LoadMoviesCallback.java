package com.digitalwood.dvdqueue.queue.model;

import com.digitalwood.dvdqueue.model.Movie;

import java.util.List;

/**
 * Created by Andrew on 11/26/2016.
 * Copyright 2016
 */

public interface LoadMoviesCallback {
    void onSuccess(List<Movie> movieList);
    void onError(String message);
}
