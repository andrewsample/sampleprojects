package com.digitalwood.dvdqueue.queue.model.storage;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.support.annotation.NonNull;
import android.util.Log;

import com.digitalwood.dvdqueue.model.Movie;
import com.digitalwood.dvdqueue.queue.model.LoadMoviesCallback;
import com.digitalwood.dvdqueue.queue.model.QueueModel;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Andrew on 12/10/2016.
 * Copyright 2016
 */

public class DatabaseSource extends ISource {

    public DatabaseSource(Context appContext) {
        super(appContext);
    }

    @Override
    public void loadMovies(final LoadMoviesCallback cbk) {
        List<Movie> movieList = new ArrayList<>();
        MovieListDbHelper dbHelper = getMovieListDbHelper();
        SQLiteDatabase db = dbHelper.getReadableDatabase();
        Cursor cursor = db.rawQuery("SELECT * FROM " + MovieListContract.MovieTable.TABLE_NAME
                + " WHERE " + MovieListContract.MovieTable.COLUMN_NAME_REMOVED + " = 0"
                + " ORDER BY " + MovieListContract.MovieTable.COLUMN_NAME_RELEASE_DATE + " DESC", null);
        try {
            while (cursor.moveToNext()) {
                Movie movie = new Movie();
                movie.setTitle(cursor.getString(cursor.getColumnIndexOrThrow(MovieListContract.MovieTable.COLUMN_NAME_TITLE)));
                movie.setImgUrl(cursor.getString(cursor.getColumnIndexOrThrow(MovieListContract.MovieTable.COLUMN_NAME_IMG_URL)));
                movie.setReleaseDate(cursor.getString(cursor.getColumnIndexOrThrow(MovieListContract.MovieTable.COLUMN_NAME_RELEASE_DATE)));
                movie.setImdbScore(cursor.getString(cursor.getColumnIndexOrThrow(MovieListContract.MovieTable.COLUMN_NAME_IMDB_SCORE)));
                movie.setParentalRating(cursor.getString(cursor.getColumnIndexOrThrow(MovieListContract.MovieTable.COLUMN_NAME_PARENTAL_RATING)));
                movieList.add(movie);
            }
            cbk.onSuccess(movieList);
        } catch (Exception e) {
            cbk.onError(e.getMessage());
        } finally {
            cursor.close();
            db.close();
        }
    }

    @NonNull
    protected MovieListDbHelper getMovieListDbHelper() {
        return new MovieListDbHelper(appContext);
    }

    public void saveMovies(List<Movie> movieList) {
        MovieListDbHelper dbHelper = getMovieListDbHelper();
        SQLiteDatabase db = dbHelper.getWritableDatabase();
        try {
            for (Movie movie : movieList) {
                ContentValues values = new ContentValues();
                values.put(MovieListContract.MovieTable.COLUMN_NAME_TITLE, movie.getTitle());
                values.put(MovieListContract.MovieTable.COLUMN_NAME_IMG_URL, movie.getImgUrl());
                values.put(MovieListContract.MovieTable.COLUMN_NAME_RELEASE_DATE, movie.getReleaseDate());
                values.put(MovieListContract.MovieTable.COLUMN_NAME_IMDB_SCORE, movie.getImdbScore());
                values.put(MovieListContract.MovieTable.COLUMN_NAME_PARENTAL_RATING, movie.getParentalRating());
                values.put(MovieListContract.MovieTable.COLUMN_NAME_REMOVED, 0);
                db.insertOrThrow(MovieListContract.MovieTable.TABLE_NAME, null, values);
            }
        } catch (Exception e) {
            Log.e(QueueModel.class.getSimpleName(), e.getLocalizedMessage());
        } finally {
            db.close();
        }
    }

    public void removeMovieFromQueue(Movie toDelete) {
        MovieListDbHelper dbHelper = getMovieListDbHelper();
        SQLiteDatabase db = dbHelper.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put(MovieListContract.MovieTable.COLUMN_NAME_REMOVED, 1);
        db.update(MovieListContract.MovieTable.TABLE_NAME,
                values,
                MovieListContract.MovieTable.COLUMN_NAME_TITLE + "=?",
                new String[] { toDelete.getTitle() });
        db.close();
    }

    public long getMsSinceLastSync() {
        MovieListDbHelper dbHelper = new MovieListDbHelper(getAppContext());
        SQLiteDatabase db = dbHelper.getReadableDatabase();
        Cursor cursor = db.rawQuery("SELECT * FROM " + MovieListContract.SyncTable.TABLE_NAME, null);
        long lastSyncTime = 0;
        try {
            if (cursor.moveToFirst()) {
                lastSyncTime = cursor.getLong(cursor.getColumnIndexOrThrow(MovieListContract.SyncTable.COLUMN_NAME_LAST_SYNC));
            }
        } finally {
            cursor.close();
            db.close();
        }
        return System.currentTimeMillis() - lastSyncTime;
    }

    public void updateLastSyncTime() {
        long currentTime = System.currentTimeMillis();
        MovieListDbHelper dbHelper = new MovieListDbHelper(getAppContext());
        SQLiteDatabase db = dbHelper.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put(MovieListContract.SyncTable.COLUMN_NAME_LAST_SYNC, currentTime);
        db.replace(MovieListContract.SyncTable.TABLE_NAME, null, values);
        db.close();
        Log.e("TESTTEST", "Sync set: " + currentTime);
    }
}
