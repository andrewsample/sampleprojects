package com.digitalwood.dvdqueue.queue.model;

import android.support.annotation.NonNull;

import com.digitalwood.dvdqueue.model.Movie;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Created by Andrew on 11/27/2016.
 * Copyright 2016
 */

public class DvdPageParser {

    private String html;
    private List<Movie> movieList;

    public DvdPageParser() {
        this("");
    }

    public DvdPageParser(@NonNull String html) {
        this.html = html;
        this.movieList = new ArrayList<>();
    }

    public void setHtml(@NonNull String html) {
        this.html = html;
    }

    public List<Movie> getMovieList() {
        movieList.clear();
        Document doc = Jsoup.parse(html);

        Elements weeks = doc.getElementsByClass("fieldtable-inner");
        for (Element week : weeks) {
            String releaseDate = getReleaseDate(week);
            addMoviesForWeek(week, releaseDate);
        }

        return movieList;
    }

    String getReleaseDate(Element week) {
        String fullReleaseDate = week.getElementsByTag("td").get(0).text(); // e.g. Tuesday December 27, 2016 (in 2 weeks)
        fullReleaseDate = fullReleaseDate.substring(0, fullReleaseDate.indexOf(" (")); // e.g. Tuesday December 27, 2016
        SimpleDateFormat inFormatter = new SimpleDateFormat("E MMM dd, yyyy");
        SimpleDateFormat outFormatter = new SimpleDateFormat("yyyy-MM-dd");
        try {
            Date date = inFormatter.parse(fullReleaseDate);
            String releaseDate = outFormatter.format(date);
            return releaseDate;
        } catch (ParseException e) {
            e.printStackTrace();
            return "";
        }
    }

    void addMoviesForWeek(Element week, String releaseDate) {
        Elements sections = week.getElementsByClass("dvdcell");
        for (Element section : sections) {
            String title = getTitle(section);
            String imgUrl = getImgUrl(section);
            String imdbScore = getImdbScore(section);
            String parentalRating = getParentalRating(section);
            Movie movie = new Movie(title, imgUrl, releaseDate, imdbScore, parentalRating);
            movieList.add(movie);
        }
    }

    private String getTitle(Element section) {
        Elements anchors = section.getElementsByTag("a");
        String title = "";
        try {
            title = anchors.get(1).text();
        } catch (Exception e) {
        }
        return title;
    }

    private String getImgUrl(Element section) {
        Elements imgs = section.getElementsByTag("img");
        String imgUrl = "";
        try {
            imgUrl= imgs.get(0).attr("src");
        } catch (Exception e) {
        }
        return imgUrl;
    }

    private String getImdbScore(Element section) {
        Elements anchors = section.getElementsByTag("a");
        String imdbScore = "";
        try {
            imdbScore = anchors.get(2).text();
        } catch (Exception e) {
        }
        return imdbScore;
    }

    private String getParentalRating(Element section) {
        Elements classes = section.getElementsByTag("td");
        String parentalRating = "";
        try {
            parentalRating = classes.get(2).text();
        } catch (Exception e) {
        }
        return parentalRating;
    }
}
