package com.digitalwood.dvdqueue.queue;

import android.net.Uri;

import com.digitalwood.dvdqueue.model.Movie;

import java.util.List;

/**
 * Created by Andrew on 11/25/2016.
 * Copyright 2016
 */

public interface QueueView {
    void setMovieList(List<Movie> movieList);

    void launchBrowser(String urlToEncode);

    Movie getMovieAtPosition(int adapterPosition);

    void removeMovieAtPosition(int adapterPosition);
}
