package com.digitalwood.dvdqueue.queue.model.storage;

import android.content.Context;

import com.digitalwood.dvdqueue.queue.model.LoadMoviesCallback;

import java.util.List;

/**
 * Created by Andrew on 12/10/2016.
 * Copyright 2016
 */

abstract class ISource {
    Context appContext;

    ISource(Context appContext) {
        this.appContext = appContext.getApplicationContext();
    }

    public Context getAppContext() {
        return appContext;
    }

    abstract void loadMovies(final LoadMoviesCallback cbk);
}
