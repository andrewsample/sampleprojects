package com.digitalwood.dvdqueue.queue.model;

import android.content.Context;
import android.support.annotation.VisibleForTesting;

import com.digitalwood.dvdqueue.model.Movie;

import com.digitalwood.dvdqueue.queue.model.storage.DatabaseSource;
import com.digitalwood.dvdqueue.queue.model.storage.WebSource;

import java.util.List;

/**
 * Created by Andrew on 11/26/2016.
 * Copyright 2016
 */

public class QueueModel {
    private static final long DEFAULT_SYNC_DELAY_MS = 1000 * 60 * 60 * 24; // 24 hours
    private boolean wasLastLoadDoneFromDatabase;
    private long syncDelayMs;
    private DatabaseSource dbStorage;
    private WebSource mWebSource;

    public QueueModel(Context appContext) {
        appContext = appContext.getApplicationContext();
        this.dbStorage = new DatabaseSource(appContext);
        this.mWebSource = new WebSource(appContext);
        this.syncDelayMs = DEFAULT_SYNC_DELAY_MS;
    }

    @VisibleForTesting
    public QueueModel(Context appContext, DatabaseSource dbStorage, WebSource webSource) {
        this.dbStorage = dbStorage;
        this.mWebSource = webSource;
        this.syncDelayMs = DEFAULT_SYNC_DELAY_MS;
    }

    public void loadMovies(final LoadMoviesCallback cbk) {
        if (hasSynchedRecently()) {
            wasLastLoadDoneFromDatabase = true;
            dbStorage.loadMovies(cbk);
        } else {
            wasLastLoadDoneFromDatabase = false;
            mWebSource.loadMovies(new LoadMoviesCallback() {
                @Override
                public void onSuccess(List<Movie> movieList) {
                    setHasSynchedRecently();
                    dbStorage.saveMovies(movieList);
                    dbStorage.loadMovies(cbk);
                }

                @Override
                public void onError(String message) {
                    cbk.onError(message);
                    dbStorage.loadMovies(cbk);
                }
            });
        }
    }

    public void removeMovie(Movie movie) {
        dbStorage.removeMovieFromQueue(movie);
    }

    private boolean hasSynchedRecently() {
        return dbStorage.getMsSinceLastSync() < syncDelayMs;
    }

    private void setHasSynchedRecently() {
        dbStorage.updateLastSyncTime();
    }

    @VisibleForTesting
    boolean wasLastLoadDoneFromDatabase() {
        return wasLastLoadDoneFromDatabase;
    }
}
