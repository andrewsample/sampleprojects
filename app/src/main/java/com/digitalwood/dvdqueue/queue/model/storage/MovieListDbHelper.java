package com.digitalwood.dvdqueue.queue.model.storage;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.support.annotation.VisibleForTesting;

import static com.digitalwood.dvdqueue.queue.model.storage.MovieListContract.*;

/**
 * Created by Andrew on 12/9/2016.
 * Copyright 2016
 */

public class MovieListDbHelper extends SQLiteOpenHelper {

    public static final String DB_NAME = "MovieList.db";
    private static final int DB_VERSION = 3;
    private static final String SQL_CREATE_MOVIE_TABLE =
            "CREATE TABLE " + MovieTable.TABLE_NAME
            + "("
            + MovieTable._ID + " INTEGER PRIMARY KEY,"
            + MovieTable.COLUMN_NAME_TITLE + " TEXT UNIQUE,"
            + MovieTable.COLUMN_NAME_IMG_URL + " TEXT,"
            + MovieTable.COLUMN_NAME_RELEASE_DATE + " TEXT,"
            + MovieTable.COLUMN_NAME_IMDB_SCORE + " TEXT,"
            + MovieTable.COLUMN_NAME_PARENTAL_RATING + " TEXT,"
            + MovieTable.COLUMN_NAME_REMOVED + " INTEGER"
            + ")";
    private static final String SQL_DELETE_MOVIE_TABLE =
            "DROP TABLE IF EXISTS " + MovieTable.TABLE_NAME;
    private static final String SQL_CREATE_SYNC_TABLE =
            "CREATE TABLE " + SyncTable.TABLE_NAME
            + "("
            + SyncTable.COLUMN_NAME_LAST_SYNC + " INTEGER"
            + ")";
    private static final String SQL_DELETE_SYNC_TABLE =
            "DROP TABLE IF EXISTS " + SyncTable.TABLE_NAME;

    public MovieListDbHelper(Context context) {
        super(context, DB_NAME, null, DB_VERSION);
    }

    @VisibleForTesting
    public MovieListDbHelper(Context context, String dbName) {
        super(context, dbName, null, DB_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(SQL_CREATE_MOVIE_TABLE);
        db.execSQL(SQL_CREATE_SYNC_TABLE);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL(SQL_DELETE_MOVIE_TABLE);
        db.execSQL(SQL_DELETE_SYNC_TABLE);
        onCreate(db);
    }
}
