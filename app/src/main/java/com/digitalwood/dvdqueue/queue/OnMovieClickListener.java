package com.digitalwood.dvdqueue.queue;

import com.digitalwood.dvdqueue.model.Movie;

/**
 * Created by Andrew on 11/30/2016.
 * Copyright 2016
 */

public interface OnMovieClickListener {
    void onClick(Movie movie);
}
