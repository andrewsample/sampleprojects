If you love movies, but can't make it to the theater regularly, use DvdQueue to keep track of movies you'd like to rent. It lists all the movies slated for release on DVD, with an IMDB score, parental guidance rating, and links to RedBox. To remove items, simply swipe them away.

Developed for Android using Model-View-Presenter with unit and end-to-end tests.